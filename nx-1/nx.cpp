/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/nx.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-21
 */

#include "DataFileLoader.h"
#include "Node.h"
#include <iostream>
#include <string>
#include <memory>
#include <fstream>
#include <map>
#include <sstream>



int printNodes(const std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
               const std::map<std::string, std::shared_ptr<Node>>& aNodesByText);
int addNode(const unsigned long& nId,
            const std::string& strText,
            std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
            std::map<std::string, std::shared_ptr<Node>>& aNodesByText);



int main(int argc, char* argv[])
{
    std::cout << "nx Copyright (C) 2021 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/hypertext-systems/nx/ and the project\n"
              << "website https://hypertext-systems.org.\n"
              << std::endl;

    std::string strDatabasePath;

    if (argc >= 2)
    {
        strDatabasePath = argv[1];
    }
    else
    {
        strDatabasePath = "./nx.csv";
    }

    std::map<unsigned long, std::shared_ptr<Node>> aNodesById;
    std::map<std::string, std::shared_ptr<Node>> aNodesByText;
    unsigned long nIdMax = 0UL;

    {
        std::unique_ptr<std::ifstream> pStream;

        try
        {
            pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
            pStream->open(strDatabasePath, std::ios::in | std::ios::binary);

            if (pStream->is_open() != true)
            {
                std::cerr << "Failed to open input file \"" << strDatabasePath << "\"." << std::endl;
                return 1;
            }

            DataFileLoader aLoader(*pStream);

            if (aLoader.load(aNodesById, aNodesByText, nIdMax) != 0)
            {
                std::stringstream strMessage;
                strMessage << "Failed to load input file \"" << strDatabasePath << "\".";

                throw new std::runtime_error(strMessage.str());
            }

            pStream->close();
        }
        catch (std::exception* pException)
        {
            std::cerr << "Exception: " << pException->what() << std::endl;

            if (pStream != nullptr)
            {
                if (pStream->is_open() == true)
                {
                    pStream->close();
                }
            }

            return 1;
        }
    }

    //printNodes(aNodesById, aNodesByText);

    std::string strInput;

    std::getline(std::cin, strInput, '\n');

    std::cout << "\n";

    if (strInput.empty() == true)
    {
        return 0;
    }

    ++nIdMax;

    addNode(nIdMax, strInput, aNodesById, aNodesByText);

    unsigned long nCount = 1UL;

    do
    {
        std::cout << nCount << ": ";

        std::getline(std::cin, strInput, '\n');

        if (strInput.empty() == true)
        {
            break;
        }

        ++nIdMax;

        addNode(nIdMax, strInput, aNodesById, aNodesByText);

        ++nCount;

    } while (true);

    std::cout << std::endl;

    //printNodes(aNodesById, aNodesByText);

    {
        /** @todo Backup before overwrite! */

        std::unique_ptr<std::ofstream> pStream;

        try
        {
            pStream = std::unique_ptr<std::ofstream>(new std::ofstream);
            pStream->open(strDatabasePath, std::ios::out | std::ios::binary | std::ofstream::trunc);

            if (pStream->is_open() != true)
            {
                std::cerr << "Failed to open output file \"" << strDatabasePath << "\"." << std::endl;
                return 1;
            }

            for (std::map<unsigned long, std::shared_ptr<Node>>::iterator iter = aNodesById.begin();
                iter != aNodesById.end();
                iter++)
            {
                *pStream << iter->first << ",\"";

                const std::string& strField = *(iter->second->GetText());

                for (std::string::const_iterator iterCharacter = strField.begin();
                    iterCharacter != strField.end();
                    iterCharacter++)
                {
                    switch (*iterCharacter)
                    {
                    case '\"':
                        *pStream << "\"\"";
                        break;
                    default:
                        *pStream << *iterCharacter;
                        break;
                    }
                }

                *pStream << "\"\r\n";
            }

            pStream->close();
        }
        catch (std::exception* pException)
        {
            std::cerr << "Exception: " << pException->what() << std::endl;

            if (pStream != nullptr)
            {
                if (pStream->is_open() == true)
                {
                    pStream->close();
                }
            }

            return 1;
        }
    }

    return 0;
}

int printNodes(const std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
               const std::map<std::string, std::shared_ptr<Node>>& aNodesByText)
{
    std::cout << " --- \n";

    for (std::map<unsigned long, std::shared_ptr<Node>>::const_iterator iter = aNodesById.begin();
         iter != aNodesById.end();
         iter++)
    {
        std::cout << iter->first << ": " << *(iter->second->GetText()) << std::endl;
    }

    std::cout << " --- " << std::endl;

    for (std::map<std::string, std::shared_ptr<Node>>::const_iterator iter = aNodesByText.begin();
         iter != aNodesByText.end();
         iter++)
    {
        std::cout << iter->first << ": " << (*iter->second->GetId()) << std::endl;
    }

    std::cout << " --- " << std::endl;

    return 0;
}

int addNode(const unsigned long& nId,
            const std::string& strText,
            std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
            std::map<std::string, std::shared_ptr<Node>>& aNodesByText)
{
    if (aNodesById.find(nId) != aNodesById.end())
    {
        std::stringstream strMessage;
        strMessage << "Duplicate ID " << nId << ".";

        throw new std::runtime_error(strMessage.str());
    }

    std::shared_ptr<Node> pNode(new Node);

    std::pair<std::map<unsigned long, std::shared_ptr<Node>>::iterator, bool> aResultById = aNodesById.insert(std::pair<unsigned long, std::shared_ptr<Node>>(nId, pNode));
    std::pair<std::map<std::string, std::shared_ptr<Node>>::iterator, bool> aResultByText = aNodesByText.insert(std::pair<std::string, std::shared_ptr<Node>>(strText, pNode));

    // pair -> iterator -> key
    pNode->SetId(&(aResultById.first->first));
    // pair -> iterator -> key
    pNode->SetText(&(aResultByText.first->first));

    return 0;
}
