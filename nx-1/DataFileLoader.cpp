/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/DataFileLoader.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-22
 */

#include "DataFileLoader.h"
#include "./CppStAC/CSVInputFactory.h"
#include <sstream>
#include <stdexcept>

typedef std::unique_ptr<cppstac::CSVInputFactory> CSVInputFactory;
typedef std::unique_ptr<cppstac::CSVEventReader> CSVEventReader;
typedef std::unique_ptr<cppstac::CSVEvent> CSVEvent;


DataFileLoader::DataFileLoader(std::istream& aStream):
  m_aStream(aStream)
{

}

DataFileLoader::~DataFileLoader()
{

}

int DataFileLoader::load(std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
                         std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
                         unsigned long& nIdMax)
{
    nIdMax = 0UL;

    CSVInputFactory pFactory = cppstac::CSVInputFactory::newInstance();
    CSVEventReader pReader = pFactory->createCSVEventReader(m_aStream);

    while (pReader->hasNext() == true)
    {
        CSVEvent pEvent = pReader->nextEvent();

        if (pEvent->isStartLine() == true)
        {
            if (pReader->hasNext() != true)
            {
                throw new std::runtime_error("Premature end of input.");
            }

            pEvent = pReader->nextEvent();

            if (pEvent->isField() != true)
            {
                throw new std::runtime_error("Line missing a field.");
            }

            unsigned long nId(DataFileLoader::ParseNumber(pEvent->asField().GetData()));

            if (aNodesById.find(nId) != aNodesById.end())
            {
                std::stringstream strMessage;
                strMessage << "Duplicate ID " << nId << ".";

                throw new std::runtime_error(strMessage.str());
            }

            if (pReader->hasNext() != true)
            {
                throw new std::runtime_error("Premature end of input.");
            }

            pEvent = pReader->nextEvent();

            if (pEvent->isField() != true)
            {
                throw new std::runtime_error("Line missing a field.");
            }

            if (nId > nIdMax)
            {
                nIdMax = nId;
            }

            std::string strText(pEvent->asField().GetData());

            std::shared_ptr<Node> pNode(new Node);

            std::pair<std::map<unsigned long, std::shared_ptr<Node>>::iterator, bool> aResultById = aNodesById.insert(std::pair<unsigned long, std::shared_ptr<Node>>(nId, pNode));
            std::pair<std::map<std::string, std::shared_ptr<Node>>::iterator, bool> aResultByText = aNodesByText.insert(std::pair<std::string, std::shared_ptr<Node>>(strText, pNode));

            // pair -> iterator -> key
            pNode->SetId(&(aResultById.first->first));
            // pair -> iterator -> key
            pNode->SetText(&(aResultByText.first->first));
        }
    }

    return 0;
}

unsigned long DataFileLoader::ParseNumber(const std::string& strInput)
{
    unsigned long nResult = 0UL;

    std::stringstream aConverter;
    aConverter << strInput;
    aConverter >> nResult;

    if (aConverter.fail() != false)
    {
        throw new std::runtime_error("Converting string to unsigned long failed.");
    }

    return nResult;
}
