# Copyright (C) 2021  Stephan Kreutzer
#
# This file is part of nx.
#
# nx is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# nx is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with nx. If not, see <http://www.gnu.org/licenses/>.



.PHONY: all build clean



CXX = g++
CXXFLAGS = -std=c++11 -Wall -Werror -Wextra -pedantic



all: ./CppStAC/cppstac.o build
build: nx



OBJECTS = nx.o ./CppStAC/cppstac.o DataFileLoader.o Node.o

.SUFFIXES: .cpp
.cpp.o:
	$(CXX) $< -c $(CXXFLAGS)



./CppStAC/cppstac.o:
	$(MAKE) --directory=./CppStAC

nx: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(CXXFLAGS)

nx.o: nx.cpp DataFileLoader.h
DataFileLoader.o: DataFileLoader.cpp DataFileLoader.h
Node.o: Node.cpp Node.h

clean:
	rm -f nx $(OBJECTS)
