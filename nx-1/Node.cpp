/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Node.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-22
 */

#include "Node.h"


Node::Node()
{

}

Node::~Node()
{

}

int Node::SetId(const unsigned long* pId)
{
    if (pId == nullptr)
    {
        throw new std::invalid_argument("Node::SetId() with pId == nullptr.");
    }

    if (m_pId != nullptr)
    {
        throw new std::runtime_error("Node::SetId() while m_pId != nullptr.");
    }

    m_pId = pId;
    return 0;
}

int Node::SetText(const std::string* pText)
{
    if (pText == nullptr)
    {
        throw new std::invalid_argument("Node::SetText() with pText == nullptr.");
    }

    if (m_pText != nullptr)
    {
        throw new std::runtime_error("Node::SetText() while m_pText != nullptr.");
    }

    m_pText = pText;

    return 0;
}

const unsigned long* Node::GetId()
{
    if (m_pId == nullptr)
    {
        throw new std::runtime_error("Node::GetId() while m_pId == nullptr.");
    }

    return m_pId;
}

const std::string* Node::GetText()
{
    if (m_pText == nullptr)
    {
        throw new std::runtime_error("Node::GetText() while m_pText == nullptr.");
    }

    return m_pText;
}
