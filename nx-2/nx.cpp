/* Copyright (C) 2021-2023 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/nx.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-21
 */

#include "DataFileLoader.h"
#include "Node.h"
#include <string>
#include <memory>
#include <fstream>
#include <map>
#include <sstream>
#include <curses.h>
#include <cstring>
#include <deque>



int printNodes(WINDOW* pWindow,
               const std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
               const std::map<std::string, std::shared_ptr<Node>>& aNodesByText);
int addNode(const unsigned long& nId,
            const std::string& strText,
            std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
            std::map<std::string, std::shared_ptr<Node>>& aNodesByText);

int printw(const std::string& strInput);
int getline(WINDOW* pWindow,
            const std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
            std::string& strLine);
int autocomplete(int nDirection,
                 const std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
                 std::string& strNeedle,
                 int nAmount,
                 const std::unique_ptr<std::deque<std::shared_ptr<Node>>>& pCandidatesBefore,
                 const std::unique_ptr<std::deque<std::shared_ptr<Node>>>& pCandidatesAfter);


int main(int argc, char* argv[])
{
    std::string strCopyrightNotice("nx Copyright (C) 2021-2023 Stephan Kreutzer\n"
                                   "This program comes with ABSOLUTELY NO WARRANTY.\n"
                                   "This is free software, and you are welcome to redistribute it\n"
                                   "under certain conditions. See the GNU Affero General Public License 3\n"
                                   "or any later version for details. Also, see the source code repository\n"
                                   "https://gitlab.com/hypertext-systems/nx/ and the project\n"
                                   "website https://hypertext-systems.org.\n\n");

    WINDOW* pWindow = initscr();
    /** @todo wprintw() for std::string, no c_str(). printw() for std::string not needed any more? */
    wprintw(pWindow, "%s", strCopyrightNotice.c_str());

    std::string strDatabasePath;

    if (argc >= 2)
    {
        strDatabasePath = argv[1];
    }
    else
    {
        strDatabasePath = "./nx.csv";
    }

    std::map<unsigned long, std::shared_ptr<Node>> aNodesById;
    std::map<std::string, std::shared_ptr<Node>> aNodesByText;
    unsigned long nIdMax = 0UL;

    {
        std::unique_ptr<std::ifstream> pStream;

        try
        {
            pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
            pStream->open(strDatabasePath, std::ios::in | std::ios::binary);

            if (pStream->is_open() != true)
            {
                wprintw(pWindow, "Failed to open input file \"%s\".\n", strDatabasePath.c_str());

                refresh();
                endwin();

                return 1;
            }

            DataFileLoader aLoader(*pStream);

            if (aLoader.load(aNodesById, aNodesByText, nIdMax) != 0)
            {
                std::stringstream strMessage;
                strMessage << "Failed to load input file \"" << strDatabasePath << "\".";

                throw new std::runtime_error(strMessage.str());
            }

            pStream->close();
        }
        catch (std::exception* pException)
        {
            wprintw(pWindow, "Exception: %s\n", pException->what());
            refresh();

            if (pStream != nullptr)
            {
                if (pStream->is_open() == true)
                {
                    pStream->close();
                }
            }

            endwin();

            return 1;
        }
    }


#if 0
    {
        int nY = 0, nX = 0;

        getyx(pWindow, nY, nX);

        int nMaxY = LINES - nY;

        // lines, cols, y, x
        WINDOW* pWindow2 = subwin(pWindow, 0, 0, nY, 0);

        if (pWindow2 == nullptr)
        {

        }

        std::unique_ptr<std::deque<std::shared_ptr<Node>>> pCandidatesBefore(new std::deque<std::shared_ptr<Node>>);
        std::unique_ptr<std::deque<std::shared_ptr<Node>>> pCandidatesAfter(new std::deque<std::shared_ptr<Node>>);

        /** @todo Avoid empty aNodesByText. */
        std::string strFirstEntry = *(aNodesByText.begin()->second->GetText());

        autocomplete(1, aNodesByText, strFirstEntry, nMaxY, pCandidatesBefore, pCandidatesAfter);

        /** @todo c_str() */

        if (pCandidatesBefore->size() > 0)
        {
            for (std::size_t i = 0U, max = pCandidatesBefore->size(); i < max; i++)
            {
                if (i == (max - 1))
                {
                    if ((*(pCandidatesBefore->at(i))->GetText()) == strFirstEntry)
                    {
                        wprintw(pWindow2, "> %s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                    }
                    else
                    {
                        wprintw(pWindow2, "  %s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                        wprintw(pWindow2, "  %s\n", strFirstEntry.c_str());
                    }
                }
                else
                {
                    wprintw(pWindow2, "  %s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                }
            }
        }
        else
        {
            wprintw(pWindow2, "  %s\n", strFirstEntry.c_str());
        }

        std::size_t nMaxAfter = nMaxY - pCandidatesBefore->size();

        for (std::size_t i = 0U, max = pCandidatesAfter->size(); i < nMaxAfter; i++)
        {
            if (i < max)
            {
                wprintw(pWindow2, "  %s\n", (pCandidatesAfter->at(i))->GetText()->c_str());
            }
            else
            {
                wprintw(pWindow2, "\n");
            }
        }

        wrefresh(pWindow2);


        getch();

        delwin(pWindow2);

        /** @todo Everywhere wrefresh(pWindow)? */
        refresh();
    }

    endwin();

    return 0;
#endif


    //printNodes(pWindow, aNodesById, aNodesByText);

    std::string strLine;

    int nResult = getline(pWindow, aNodesByText, strLine);

    /** @todo Handling first vs. later */
    if (nResult == 0)
    {
        
    }
    else if (nResult == 1)
    {
        // enter, exact match
    }
    else if (nResult == 2)
    {
        // enter, not exact match
    }
    else if (nResult == 3)
    {
        // q
    }
    else
    {
        /** @todo Proper error handling. */
        strLine = "";
    }

    wprintw(pWindow, "\n\n");
    refresh();

    if (strLine.empty() == true)
    {
        endwin();

        return 0;
    }

    ++nIdMax;

    addNode(nIdMax, strLine, aNodesById, aNodesByText);
    strLine = "";

    unsigned long nCount = 1UL;

    do
    {
        std::string strInput;

        clrtoeol();

        {
            int x = 0;
            int y = 0;

            getyx(pWindow, y, x);

            /** @todo c_str() */
            mvwprintw(pWindow, y, 0, "%lu: %s", nCount, strLine.c_str());
        }

        refresh();

        nResult = getline(pWindow, aNodesByText, strInput);

        if (nResult == 0)
        {
            strLine += strInput;
        }
        else if (nResult == 1)
        {
            // enter, exact match
            strLine += strInput;
        }
        else if (nResult == 2)
        {
            // enter, not exact match
            strLine += strInput;
        }
        else if (nResult == 3)
        {
            // q
            deleteln();
            refresh();

            /** @todo Problem: next iteration in autocomplete doesn't have previous inut. */

            strLine += strInput;

            continue;
        }
        else
        {
            /** @todo Proper error handling. */
            strLine = "";
        }

        if (strLine.empty() == true)
        {
            break;
        }

        wprintw(pWindow, "\n");
        refresh();

        ++nIdMax;

        addNode(nIdMax, strLine, aNodesById, aNodesByText);
        strLine = "";

        ++nCount;

    } while (true);

    wprintw(pWindow, "\n");
    refresh();

    //printNodes(aNodesById, aNodesByText);

    {
        /** @todo Backup before overwrite! */

        std::unique_ptr<std::ofstream> pStream;

        try
        {
            pStream = std::unique_ptr<std::ofstream>(new std::ofstream);
            pStream->open(strDatabasePath, std::ios::out | std::ios::binary | std::ofstream::trunc);

            if (pStream->is_open() != true)
            {
                wprintw(pWindow, "Failed to open output file \"%s\".\n", strDatabasePath.c_str());
                refresh();
                endwin();

                return 1;
            }

            for (std::map<unsigned long, std::shared_ptr<Node>>::iterator iter = aNodesById.begin();
                iter != aNodesById.end();
                iter++)
            {
                *pStream << iter->first << ",\"";

                const std::string& strField = *(iter->second->GetText());

                for (std::string::const_iterator iterCharacter = strField.begin();
                    iterCharacter != strField.end();
                    iterCharacter++)
                {
                    switch (*iterCharacter)
                    {
                    case '\"':
                        *pStream << "\"\"";
                        break;
                    default:
                        *pStream << *iterCharacter;
                        break;
                    }
                }

                *pStream << "\"\r\n";
            }

            pStream->close();
        }
        catch (std::exception* pException)
        {
            wprintw(pWindow, "Exception: %s\n", pException->what());
            refresh();

            if (pStream != nullptr)
            {
                if (pStream->is_open() == true)
                {
                    pStream->close();
                }
            }

            endwin();

            return 1;
        }
    }

    endwin();

    return 0;
}

int printNodes(WINDOW* pWindow,
               const std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
               const std::map<std::string, std::shared_ptr<Node>>& aNodesByText)
{
    wprintw(pWindow, " --- \n");

    for (std::map<unsigned long, std::shared_ptr<Node>>::const_iterator iter = aNodesById.begin();
         iter != aNodesById.end();
         iter++)
    {
        wprintw(pWindow, "%lu: %s\n", iter->first, (iter->second->GetText())->c_str());
    }

    wprintw(pWindow, " --- \n");

    for (std::map<std::string, std::shared_ptr<Node>>::const_iterator iter = aNodesByText.begin();
         iter != aNodesByText.end();
         iter++)
    {
        wprintw(pWindow, "%s", iter->first.c_str());
        wprintw(pWindow, ": %lu\n", *(iter->second->GetId()));
    }

    wprintw(pWindow, " --- \n");
    refresh();

    return 0;
}

int addNode(const unsigned long& nId,
            const std::string& strText,
            std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
            std::map<std::string, std::shared_ptr<Node>>& aNodesByText)
{
    if (aNodesById.find(nId) != aNodesById.end())
    {
        std::stringstream strMessage;
        strMessage << "Duplicate ID " << nId << ".";

        throw new std::runtime_error(strMessage.str());
    }

    std::shared_ptr<Node> pNode(new Node);

    std::pair<std::map<unsigned long, std::shared_ptr<Node>>::iterator, bool> aResultById = aNodesById.insert(std::pair<unsigned long, std::shared_ptr<Node>>(nId, pNode));
    std::pair<std::map<std::string, std::shared_ptr<Node>>::iterator, bool> aResultByText = aNodesByText.insert(std::pair<std::string, std::shared_ptr<Node>>(strText, pNode));

    // pair -> iterator -> key
    pNode->SetId(&(aResultById.first->first));
    // pair -> iterator -> key
    pNode->SetText(&(aResultByText.first->first));

    return 0;
}

int printw(const std::string& strInput)
{
    // Make strInput.c_str() stable as
    // unknown/non-quaranteed which operations
    // std::strncpy() will perform on it.
    std::unique_ptr<char> pStrInput;
    std::size_t nLength(strInput.size());
    pStrInput.reset(new char[nLength + 1U]);

    if (nLength > 0U)
    {
        std::strncpy(pStrInput.get(), strInput.c_str(), nLength);
    }

    pStrInput.get()[nLength] = '\0';
    printw("%s", pStrInput.get());
    refresh();

    return 0;
}

/** @todo This keeps line wraps and moves the next printw()/getline() down,
  * except for the last explicit input line break nCharacter == 0xA. */
int getline(WINDOW* pWindow,
            const std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
            std::string& strLine)
{
    int nResult = 0;

    std::stringstream str;

    keypad(pWindow, TRUE);
    noecho();

    do
    {
        int nCharacter(getch());

        if (nCharacter == ERR)
        {
            echo();
            keypad(pWindow, FALSE);
            return 1;
        }

        if (nCharacter == 0xA)
        {
            echo();
            keypad(pWindow, FALSE);
            break;
        }

        if (nCharacter == KEY_UP ||
            nCharacter == KEY_DOWN)
        {
            std::string strLineBefore = str.str();

            std::unique_ptr<std::deque<std::shared_ptr<Node>>> pCandidatesBefore(new std::deque<std::shared_ptr<Node>>);
            std::unique_ptr<std::deque<std::shared_ptr<Node>>> pCandidatesAfter(new std::deque<std::shared_ptr<Node>>);

            int nMaxY = LINES / 2;

            // lines, cols, y, x
            WINDOW* pWindow2 = subwin(pWindow, nMaxY, COLS, LINES - nMaxY, 0);

            if (pWindow2 == nullptr)
            {
                
            }

            if (has_colors() == TRUE)
            {
                int nStartColorResult = start_color();

                if (nStartColorResult == OK)
                {
                    init_pair(1, COLOR_YELLOW, COLOR_BLUE);
                    //attron(COLOR_PAIR(1));
                    wbkgd(pWindow2, COLOR_PAIR(1));
                }
                else if (nStartColorResult == ERR)
                {
                    
                }
                else
                {
                    
                }
            }
            else
            {
                wprintw(pWindow2, "--------------------------");
            }

            keypad(pWindow, FALSE);
            keypad(pWindow2, TRUE);
            //noecho();

            strLine = str.str();
            str.str("");

            do
            {
                if (nCharacter == KEY_UP)
                {
                    autocomplete(1, aNodesByText, strLine, nMaxY, pCandidatesBefore, pCandidatesAfter);
                }
                else if (nCharacter == KEY_DOWN)
                {
                    autocomplete(1, aNodesByText, strLine, nMaxY, pCandidatesBefore, pCandidatesAfter);
                }
                else
                {
                    
                }


                int nOffsetNewEntry = 0;
                bool bIsExactMatch = false;

                /** @todo c_str() */

                if (pCandidatesBefore->size() > 0)
                {
                    for (std::size_t i = 0U, max = pCandidatesBefore->size(); i < max; i++)
                    {
                        if (i == (max - 1))
                        {
                            if ((*(pCandidatesBefore->at(i))->GetText()) == strLine)
                            {
                                wprintw(pWindow2, "===%s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                                bIsExactMatch = true;
                            }
                            else
                            {
                                wprintw(pWindow2, "---%s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                                wprintw(pWindow2, ">>>%s\n", strLine.c_str());

                                nOffsetNewEntry = 1;
                            }
                        }
                        else
                        {
                            wprintw(pWindow2, "---%s\n", (pCandidatesBefore->at(i))->GetText()->c_str());
                        }
                    }
                }
                else
                {
                    wprintw(pWindow2, ">>>%s\n", strLine.c_str());

                    nOffsetNewEntry = 1;
                }

                std::size_t nMaxAfter = nMaxY - (pCandidatesBefore->size() + nOffsetNewEntry);

                for (std::size_t i = 0U, max = pCandidatesAfter->size(); i < nMaxAfter; i++)
                {
                    if (i < max)
                    {
                        wprintw(pWindow2, "+++%s\n", (pCandidatesAfter->at(i))->GetText()->c_str());
                    }
                    else
                    {
                        wprintw(pWindow2, "\n");
                    }
                }

                wrefresh(pWindow2);


                int nCharacter = wgetch(pWindow2);

                if (nCharacter == ERR)
                {
                    echo();
                    keypad(pWindow, FALSE);
                    keypad(pWindow2, FALSE);
                    delwin(pWindow2);
                    refresh();
                    return 1;
                }

                if (nCharacter == 'q' ||
                    nCharacter == 'Q')
                {
                    echo();
                    keypad(pWindow, FALSE);
                    keypad(pWindow2, FALSE);
                    delwin(pWindow2);
                    refresh();

                    strLine = strLineBefore;

                    return 3;
                }
                else if (nCharacter == 0x0A)
                {
                    str.str(strLine);

                    if (bIsExactMatch == true)
                    {
                        nResult = 1;
                    }
                    else
                    {
                        nResult = 2;
                    }

                    keypad(pWindow2, FALSE);
                    wclear(pWindow2);
                    delwin(pWindow2);
                    break;
                }
                else if (nCharacter == KEY_UP)
                {
                    std::size_t nCandidatesBeforeSize = pCandidatesBefore->size();

                    if (nCandidatesBeforeSize > 0U)
                    {
                        if (bIsExactMatch != true)
                        {
                            strLine = *(pCandidatesBefore->at(nCandidatesBeforeSize - 1U)->GetText());
                        }
                        else
                        {
                            if (nCandidatesBeforeSize > 1U)
                            {
                                strLine = *(pCandidatesBefore->at(nCandidatesBeforeSize - 2U)->GetText());
                            }
                        }
                    }

                    /** @todo Inefficient, just lazy. */
                    pCandidatesBefore->clear();
                    pCandidatesAfter->clear();

                    wclear(pWindow2);
                }
                else if (nCharacter == KEY_DOWN)
                {
                    std::size_t nCandidatesAfterSize = pCandidatesAfter->size();

                    if (nCandidatesAfterSize > 0U)
                    {
                        strLine = *(pCandidatesAfter->at(0U)->GetText());
                    }

                    /** @todo Inefficient, just lazy. */
                    pCandidatesBefore->clear();
                    pCandidatesAfter->clear();

                    wclear(pWindow2);
                }

            } while (true);

            break;
        }
        else if (nCharacter == KEY_BACKSPACE)
        {
            int x = 0;
            int y = 0;

            getyx(pWindow, y, x);

            mvcur(y, x, y + 0, x + 1);
            delch();

            refresh();
        }
        else
        {
            char cCharacter = static_cast<char>(nCharacter);
            str << cCharacter;

            addch(cCharacter);
        }

    } while (true);

    strLine = str.str();

    keypad(pWindow, FALSE);
    echo();
    refresh();

    return nResult;
}

int autocomplete(int nDirection,
                 const std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
                 std::string& strNeedle,
                 int nAmount,
                 const std::unique_ptr<std::deque<std::shared_ptr<Node>>>& pCandidatesBefore,
                 const std::unique_ptr<std::deque<std::shared_ptr<Node>>>& pCandidatesAfter)
{
    if (nDirection == 1)
    {
        std::map<std::string, std::shared_ptr<Node>>::const_iterator iterAfter = aNodesByText.upper_bound(strNeedle);

        if (iterAfter != aNodesByText.begin())
        {
            std::map<std::string, std::shared_ptr<Node>>::const_iterator iterBeforeOrOn = iterAfter;
            int nAmountBefore = nAmount / 2;

            for (int i = 0; i < nAmountBefore; i++)
            {
                --iterBeforeOrOn;
                pCandidatesBefore->push_front(iterBeforeOrOn->second);

                if (iterBeforeOrOn == aNodesByText.begin())
                {
                    break;
                }
            }
        }

        /** @todo What if aNodesByText is empty (iterAfter != aNodesByText.begin() && iterAfter == aNodesByText.end())? */

        if (iterAfter != aNodesByText.end())
        {
            int nRemainder = nAmount - pCandidatesBefore->size();

            for (int i = 0; i < nRemainder; i++)
            {
                pCandidatesAfter->push_back(iterAfter->second);
                iterAfter++;

                if (iterAfter == aNodesByText.end())
                {
                    break;
                }
            }
        }
    }
    else if (nDirection == 2)
    {

    }
    else
    {

    }

    return 0;
}
