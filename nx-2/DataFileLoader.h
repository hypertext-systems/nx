/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/DataFileLoader.h
 * @author Stephan Kreutzer
 * @since 2021-06-22
 */

#ifndef _DATAFILELOADER_H
#define _DATAFILELOADER_H

#include "Node.h"
#include <istream>
#include <map>
#include <memory>
#include <string>

class DataFileLoader
{
public:
    DataFileLoader(std::istream& aStream);
    ~DataFileLoader();

public:
    int load(std::map<unsigned long, std::shared_ptr<Node>>& aNodesById,
             std::map<std::string, std::shared_ptr<Node>>& aNodesByText,
             unsigned long& nIdMax);

public:
    static unsigned long ParseNumber(const std::string& strInput);

protected:
    std::istream& m_aStream;
    unsigned long m_nIdMax = 0UL;

};

#endif
