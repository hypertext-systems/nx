/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of nx.
 *
 * nx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * nx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with nx. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Node.h
 * @author Stephan Kreutzer
 * @since 2021-06-22
 */

#ifndef _NODE_H
#define _NODE_H

#include <string>
#include <stdexcept>

class Node
{
public:
    Node();
    ~Node();

public:
    int SetId(const unsigned long* pId);
    int SetText(const std::string* pText);

    const unsigned long* GetId();
    const std::string* GetText();

protected:
    const unsigned long* m_pId = nullptr;
    const std::string* m_pText = nullptr;

};

#endif
